import React, { useState } from "react";
import {
    Text,
    View,
    StyleSheet,
    TextInput,
    Pressable,
    ActivityIndicator,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
function Send() {
    const [toAddress, setToAddress] = useState();
    const [sendAmount, setSendAmount] = useState();
    const [sendStatus, setSendStatus] = useState(false);
    return (
        <View style={styles.rootContainer}>
            <View style={styles.headerContainer}>
                <Text style={styles.headerTitle}>Sepolia Test Network</Text>
                <Text style={styles.headerTagLine}>Send Tokens</Text>
            </View>
            <View style={styles.body}>
                {!sendStatus ? (
                    <>
                        <View style={styles.fromAddressContainer}>
                            <Text style={styles.label}>From:</Text>
                            <Pressable style={styles.accountButton}>
                                <View style={styles.balanceAccountContainer}>
                                    <Text
                                        style={styles.balanceAccountTextColor}>****Get Account
                                        Address***</Text>
                                    <Text style={styles.balanceAccountTextColor}>
                                        Balance: SeopliaETH </Text>
                                </View>
                                <Ionicons
                                    name="ios-chevron-down-circle-sharp"
                                    size={32}
                                    color="white"
                                    style={styles.dropDownIcon}
                                />
                            </Pressable>
                        </View>
                        <View style={styles.toAddressContainer}>
                            <Text style={styles.label}>To:</Text>
                            <TextInput
                                placeholder="Recipient Address"
                                style={styles.input}
                                value={toAddress}
                                onChangeText={(enteredAddress) => setToAddress(enteredAddress)}
                            />
                        </View>
                        <View style={styles.toAddressContainer}>
                            <Text style={styles.label}>Amount:</Text>
                            <TextInput
                                placeholder="Enter ammount in ethers"
                                style={styles.input}
                                value={sendAmount}
                                onChangeText={(enteredAmount) => setSendAmount(enteredAmount)}
                            />
                        </View>
                        <Pressable style={styles.sendButton}>
                            <Text style={styles.sendButtonText}>Send</Text>
                        </Pressable>
                    </>
                ) : (
                    <View style={styles.loadingScreen}>
                        <Text style={styles.laodingText}>
                            Wait Please, sending the token. It might take from
                            few second
                            to few minutes to complete.
                        </Text>
                    </View>
                )}
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    rootContainer: {
        flex: 1,
    },
    headerContainer: {
        alignItems: "center",
        backgroundColor: "#0a4f95",
        paddingVertical: 20,
    },
    headerTitle: {
        fontSize: 30,
        fontWeight: "bold",
        color: "white",
        paddingVertical: 20,
    },
    headerTagLine: {
        color: "white",
        fontSize: 20,
    },
    body: {
        marginTop: 50,
        flex: 0.5,
        justifyContent: "space-between",
    },
    accountButton: {
        flexDirection: "row",
        backgroundColor: "#0a4f95",
        paddingVertical: 10,
        paddingHorizontal: 30,
        borderRadius: 20,
    },
    balanceAccountContainer: {
        justifyContent: "center",
        alignItems: "center",

    },
    balanceAccountTextColor: {
        color: "white",
        fontSize: 15,
    },
    dropDownIcon: {
        paddingLeft: 15,
        justifyContent: "center",
        alignItems: "center",
    },
    fromAddressContainer: {
        marginTop: 10,
        flexDirection: "row",
    },
    toAddressContainer: {
        marginTop: 10,
        flexDirection: "row",
    },
    label: {
        width: "25%",
        fontSize: 18,
        fontWeight: "bold",
        paddingHorizontal: 10,
        textAlign: "right",
        paddingVertical: 20,
    },
    input: {
        flex: 1,
        backgroundColor: "white",
        marginRight: 10,
        paddingLeft: 5,
    },
    sendButton: {
        marginTop: 30,
        alignSelf: "center",
        backgroundColor: "#0a4f95",
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 30,
    },
    sendButtonText: {
        fontSize: 20,
        color: "white",
    },
});
export default Send;